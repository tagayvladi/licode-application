/*global require, __dirname, console*/
var express = require('express'),
    bodyParser = require('body-parser'),
    errorhandler = require('errorhandler'),
    morgan = require('morgan'),
    net = require('net'),
    N = require('./nuve'),
    fs = require("fs"),
    path = require('path'),
    https = require("https"),
    config = require('./../../licode_config');

var options = {
    key: fs.readFileSync('cert/key.pem').toString(),
    cert: fs.readFileSync('cert/cert.pem').toString()
};

var app = express();

// app.configure ya no existe
"use strict";
app.use(errorhandler({
    dumpExceptions: true,
    showStack: true
}));
app.use(morgan('dev'));
app.use(express.static(__dirname + '/public'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

//app.set('views', __dirname + '/../views/');
//disable layout
//app.set("view options", {layout: false});

N.API.init(config.nuve.superserviceID, config.nuve.superserviceKey, 'http://localhost:3000/');

var myRoom;

//N.API.getRooms(function(roomlist) {
//    "use strict";
//    var rooms = JSON.parse(roomlist);
//    console.log(rooms.length); //check and see if one of these rooms is 'myRoom'
//    for (var room in rooms) {
//        if (room.name === 'myRoom'){
//            myRoom = room._id;
//        }
//    }
//    if (!myRoom) {
//
//        N.API.createRoom('myRoom', function(roomID) {
//            myRoom = roomID._id;
//            console.log('Created room ', myRoom);
//        });
//    } else {
//        console.log('Using room ', myRoom);
//    }
//}, function(a) {
//    console.log(a)
//});

app.get('/', function(req, res) {
    res.render('index', { title: 'licode', user: req.user });
})

app.get('/getRooms/', function(req, res) {
    "use strict";
    N.API.getRooms(function(rooms) {
        res.send(rooms);
    });
});

app.get('/getRoom/:room', function(req, res) {
    N.API.getRoom(req.params.room, function(room) {
        if(!room)
            res.send('error');
        else
            res.send(room);
    })
})
app.post('/create', function(req, res) {
    var roomName = req.body.roomName;
    N.API.getRooms(function(rooms) {
        if(rooms.indexOf(roomName) != -1) {
            res.send('denied');
            return;
        }
        N.API.createRoom(roomName, function(roomID) {
            res.send(roomID);
            console.log('created room' + roomID);
        }, function(e){
            console.log(e);
        }, {p2p: false, data: {adminBW : 400, userBW: 1000, maxUsers: 10, chat: true }})

    })
})

app.get('/getUsers/:room', function(req, res) {
    "use strict";
    var room = req.params.room;
    N.API.getUsers(room, function(users) {
        res.send(users);
    });
});


app.post('/createToken', function(req, res) {
   // "use strict";
    var room = req.body.room,
        username = req.body.username,
        role = req.body.role;
    N.API.createToken(room, username, role, function(token) {
        console.log(token + ' FOR ROOM # ' + room);
        res.send(token);

    });
});


app.use(function(req, res, next) {
    "use strict";
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, DELETE');
    res.header('Access-Control-Allow-Headers', 'origin, content-type');
    if (req.method == 'OPTIONS') {
        res.send(200);
    } else {
        next();
    }
});



app.listen(3001);

var server = https.createServer(options, app);
server.listen(3004);