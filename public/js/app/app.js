
var WebRTCApp = angular.module('WebRTCApp', ['ngRoute', 'controllers', 'services']);

WebRTCApp.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
            when('/', {
                templateUrl: '/partials/rooms.html',
                controller: 'RoomListCtrl'}).
            when('/:id', {
                templateUrl: '/partials/conference.html',
                controller: 'ConferenceCtrl'}).
            otherwise({
                redirectUrl: '/'
            });
    }]);


