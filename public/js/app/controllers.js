var controllers = angular.module('controllers', [])

controllers.run(function($rootScope, $location, API) {
    $rootScope.isUser = true; // temporary value
    $rootScope.redirectTo = function(url) {
        $location.path(url);
    };

    $rootScope.makeID = function() {
        return Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 5);
    }
    $rootScope.APIErrorHandler = function(d, s) {
        console.log(d, s);
    }

    $rootScope.createToken = function(username, role, room, callback) {
        var obj = {
            username: username,
            role: role,
            room: room
        }
        API.request('/createToken', obj, 'POST').success(function(d, s) {
            if(d == 'error') return;
            $rootScope.token = d;
            callback()

        }).error($rootScope.APIErrorHandler);
    }

    $rootScope.getToken = function(roomID, callback) {
        if(!$rootScope.token) {
            var user = $rootScope.makeID();
            $rootScope.createToken(user, 'presenter', roomID, callback);
        }
        else
            callback();
    }


});

controllers.controller('RoomListCtrl',  ['$scope', '$rootScope', 'API',
    function($scope, $rootScope,  API) {
        $scope.rooms = [];
        $scope.newRoomName = "";
        //console.log(newRoomName);



        $scope.createToken = function(username, role, room, callback) {
            var obj = {
                username: username,
                role: role,
                room: room
            }
            API.request('/createToken', obj, 'POST').success(function(d, s) {
                if(d == 'error') return;
                $rootScope.token = d;
                callback()

            }).error($rootScope.APIErrorHandler);
        }

        $scope.createRoom = function() {
            API.request('/create', {roomName: $scope.newRoomName}, 'POST').success(function(d, s){
                if(d == 'denied')
                    console.log('Room is already exist.');
                else {
                    var roomID = d._id;
                    var user = $rootScope.makeID();
                    $scope.createToken(user, 'presenter', roomID, function() {
                        $rootScope.redirectTo('/' + roomID);
                    });
                }
            }).error($rootScope.APIErrorHandler)
        }

        $scope.getRooms = function() {
            API.request('getRooms').success(function(d, s) {
                $scope.rooms = d;
                console.log(d);
            }).error($rootScope.APIErrorHandler)
        }
        $scope.getRooms();
    }]
);

controllers.controller('ConferenceCtrl',  ['$scope', '$rootScope', 'API', '$routeParams',
    function($scope, $rootScope,  API, $routeParams){

        var roomID = $routeParams.id;
        $scope.streams = [];

        $scope.init = function() {

            $scope.subscribeToStreams = function (streams) {
                for (var index in streams) {
                    var stream = streams[index];
                    if (localStream.getID() !== stream.getID()) {
                        room.subscribe(stream);
                    }
                }
            };

            console.log($rootScope.token);

            var localStream = Erizo.Stream({video: true, audio: true, videoSize: [320, 240, 640, 480]});
            localStream.init();

            var room = Erizo.Room({token: $rootScope.token});
            room.connect();

            $scope.mute = function() {
                localStream.stream.getAudioTracks()[0].enabled = false;
            }

            $scope.unMute = function() {
                localStream.stream.getAudioTracks()[0].enabled = true;
            }


            $scope.getRoom = function(roomID) {
                API.request('/getRoom/' + roomID).success(function(d, s) {
                        if(d == 'error')
                            return;
                        $scope.roomInfo = d;
                        console.log(d);
                    }).error($rootScope.APIErrorHandler);
            }


            room.addEventListener("room-connected", function (roomEvent) {
                $scope.subscribeToStreams(roomEvent.streams);
                console.log('connected to the room');
                $scope.getRoom(room.roomID);
            });

            room.addEventListener("stream-added", function (streamEvent) {
                var streams = [];
                streams.push(streamEvent.stream);
                $scope.subscribeToStreams(streams);
            });

            room.addEventListener("stream-subscribed", function(streamEvent) {
                var stream = streamEvent.stream;

                $scope.$apply(function() {
                    $scope.streams.push(stream);
                })
                stream.show(stream.getID());
                console.log('Subscribed!')
               console.log(stream);

            });

            room.addEventListener('stream-removed', function(roomEvent) {
                var streamID = roomEvent.stream.getID();
                $scope.$apply(function() {
                    $scope.streams.forEach(function(stream) {
                        if(stream.getID() == streamID) {
                            $scope.streams.splice($scope.streams.indexOf(stream), 1);
                            return;
                        }
                    })
                })
            })

            localStream.addEventListener('access-accepted', function(event) {
                console.log("Access to webcam and microphone granted");
                localStream.show('myVideo');
                room.publish(localStream, {maxVideoBW: 300});
            });
            localStream.addEventListener('access-denied', function(event) {
                console.log("Access to webcam and microphone rejected");
            });
        }

        $rootScope.getToken(roomID, $scope.init);

    }]
);