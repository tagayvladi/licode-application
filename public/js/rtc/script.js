var selfEasyrtcid = "";

function disable(domId) {
    document.getElementById(domId).disabled = "disabled";
}


function enable(domId) {
    document.getElementById(domId).disabled = "";
}


function connect() {
    easyrtc.enableDebug(false);
    console.log("Initializing.");
    easyrtc.enableAudio(true);
    easyrtc.enableAudioReceive(true);
    easyrtc.setRoomOccupantListener(convertListToButtons);
    easyrtc.initMediaSource(
        function(){    // success callback
            var selfVideo = document.getElementById("selfVideo");
            easyrtc.setVideoObjectSrc(selfVideo, easyrtc.getLocalStream());
           // easyrtc.getRoomNames(function(a){console.dir(a)});
            easyrtc.connect("easyrtc.videoOnly", loginSuccess, loginFailure);
        },
        function(errorCode, errmesg){
            easyrtc.showError("MEDIA-ERROR", errmesg);
        }  // failure callback
    );
}


function terminatePage() {
    easyrtc.disconnect();
}


function hangup() {
    easyrtc.hangupAll();
    disable("hangupButton");
}


function clearConnectList() {
    var otherClientDiv = document.getElementById("otherClients");
    while (otherClientDiv.hasChildNodes()) {
        otherClientDiv.removeChild(otherClientDiv.lastChild);
    }

}


function convertListToButtons (roomName, occupants, isPrimary) {
    clearConnectList();
    var otherClientDiv = document.getElementById("otherClients");
    for(var easyrtcid in occupants) {
        var button = document.createElement("button");
        button.onclick = function(easyrtcid) {
            return function() {
                performCall(easyrtcid);
            };
        }(easyrtcid);

        var label = document.createTextNode( easyrtc.idToName(easyrtcid));
        button.appendChild(label);
        otherClientDiv.appendChild(button);
    }
    if( !otherClientDiv.hasChildNodes() ) {
        otherClientDiv.innerHTML = "<em>Nobody else is on...</em>";
    }
}


function performCall(otherEasyrtcid) {
    var acceptedCB = function(accepted, easyrtcid) {
        if( !accepted ) {
            easyrtc.showError("CALL-REJECTED", "Sorry, your call to " + easyrtc.idToName(easyrtcid) + " was rejected");
            enable("otherClients");
        }
    };
    var successCB = function() {
        enable("hangupButton");
    };
    var failureCB = function() {
        enable("otherClients");
    };
    easyrtc.call(otherEasyrtcid, successCB, failureCB, acceptedCB);
}


function loginSuccess(easyrtcid) {
    console.log('I am connected.');
    disable("connectButton");
    enable("otherClients");
    selfEasyrtcid = easyrtcid;
}


function loginFailure(errorCode, message) {
    easyrtc.showError(errorCode, message);
}


function disconnect() {
    document.getElementById("iam").innerHTML = "logged out";
    easyrtc.disconnect();
    console.log("disconnecting from server");
    enable("connectButton");
    clearConnectList();
    easyrtc.setVideoObjectSrc(document.getElementById("selfVideo"), "");
}

var x = 0;
easyrtc.setStreamAcceptor( function(easyrtcid, stream) {
    if(x==0) {
        var video = document.getElementById("callerVideo");
        x++;
    } else if (x==1) {
        var video = document.getElementById("callerVideo1");
    }
    easyrtc.setVideoObjectSrc(video,stream);

});


easyrtc.setOnStreamClosed( function (easyrtcid) {
    easyrtc.setVideoObjectSrc(document.getElementById("callerVideo"), "");
    easyrtc.setVideoObjectSrc(document.getElementById("callerVideo1"), "");

});


easyrtc.setAcceptChecker(function(easyrtcid, callback) {
    callback(true);

} );